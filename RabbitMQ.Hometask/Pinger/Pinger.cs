﻿using System;
using System.Text;
using System.Threading.Tasks;
using Common.Helpers;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;

namespace Pinger
{
    class Pinger
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(PingPongHelper.GetProjectDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build();
            
            var pinger = new RabbitMqService(new ConnectionSettings()
            {
                HostName = configuration.GetSection("RabbitMQConnection:Host").Value,
                QueueName = "pong_queue"
                //you can add more params, such as Uri, ExchangeName etc.
            });
            
            const string messageToSend = "ping";
            var bytesToSend = Encoding.UTF8.GetBytes(messageToSend);

            pinger.SendMessageToQueue(bytesToSend);
            
            pinger.ListenQueue("ping_queue");
            
            pinger.OnGetMessage += async (sender, eventArgs) =>
            {
                var body = eventArgs.Body.ToArray();
                var messageResult = Encoding.UTF8.GetString(body);

                PingPongHelper.DisplayPingMessage(messageResult);

                await Task.Delay(2500);

                pinger.SendMessageToQueue(bytesToSend);
            };
            
            Console.ReadLine();
            pinger.Dispose();
        }
    }
}