﻿using System;
using System.IO;

namespace Common.Helpers
{
    public static class PingPongHelper
    {
        public static void DisplayPingMessage(string message)
        {
            DisplayMessageOnConsole(message, ConsoleColor.Yellow);
        }

        public static void DisplayPongMessage(string message)
        {
            DisplayMessageOnConsole(message, ConsoleColor.Green);
        }

        private static void DisplayMessageOnConsole(string message, ConsoleColor color)
        {
            PrintColorLine("┌" + new string('─', 30) + "┐", color);
            Console.WriteLine($"{DateTime.Now:MM/dd/yyyy hh:mm:ss.fff tt}: {message}");
            PrintColorLine("└" + new string('─', 30) + "┘", color);
        }

        public static string GetProjectDirectory()
        {
            string workingDirectory = Environment.CurrentDirectory;
            return Directory.GetParent(workingDirectory)?.Parent?.Parent?.FullName;
        }

        private static void PrintColorLine(string @string, ConsoleColor color)
        {
            Console.ForegroundColor = color;
            Console.WriteLine(@string);
            Console.ResetColor();
        }
    }
}