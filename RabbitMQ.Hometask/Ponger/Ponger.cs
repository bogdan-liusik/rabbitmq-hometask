﻿using System;
using System.Text;
using System.Threading.Tasks;
using Common.Helpers;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Wrapper.Models;
using RabbitMQ.Wrapper.Services;

namespace Ponger
{
    class Ponger
    {
        static void Main(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(PingPongHelper.GetProjectDirectory())
                .AddJsonFile("appsettings.json", false)
                .Build();
            
            var ponger = new RabbitMqService(new ConnectionSettings()
            {
                HostName = configuration.GetSection("RabbitMQConnection:Host").Value,
                QueueName = "ping_queue"
                //here you can add more params, such as Uri, ExchangeName etc.
            });
            
            const string messageToSend = "pong";
            var bytesToSend = Encoding.UTF8.GetBytes(messageToSend);
            
            ponger.ListenQueue("pong_queue");

            ponger.OnGetMessage += async (sender, eventArgs) =>
            {
                var body = eventArgs.Body.ToArray();
                var messageResult = Encoding.UTF8.GetString(body);

                PingPongHelper.DisplayPongMessage(messageResult);

                await Task.Delay(2500);

                ponger.SendMessageToQueue(bytesToSend);
            };

            Console.ReadLine();
            ponger.Dispose();
        }
    }
}