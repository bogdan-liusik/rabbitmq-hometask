﻿using System;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Wrapper.Interfaces;
using RabbitMQ.Wrapper.Models;

namespace RabbitMQ.Wrapper.Services
{
    public class RabbitMqService : IRabbitMqService, IDisposable
    {
        private IModel _channel;
        private IConnection _connection;
        private EventingBasicConsumer _consumer;
        private readonly ConnectionSettings _connectionSettings;

        public event EventHandler<BasicDeliverEventArgs> OnGetMessage;

        public RabbitMqService(ConnectionSettings connectionSettings)
        {
            _connectionSettings = connectionSettings;
            Init();
        }
        
        private void Init()
        {
            var factory = new ConnectionFactory() { HostName = _connectionSettings.HostName};
            _connection = factory.CreateConnection();
            _channel = _connection.CreateModel();
            
            _channel.QueueDeclare(queue: _connectionSettings.QueueName,
                durable: false,
                exclusive: false,
                autoDelete: false,
                arguments: null);
            
            _consumer = new EventingBasicConsumer(_channel);

            _consumer.Received += (model, eventArgs) =>
            {
                OnGetMessage?.Invoke(model, eventArgs);
            };
        }

        public void ListenQueue(string queueName)
        {
            _channel.BasicConsume(queue: queueName,
                autoAck: true,
                consumer: _consumer);
        }

        public void SendMessageToQueue(byte[] message)
        {
            _channel.BasicPublish(exchange: "",
                routingKey: _connectionSettings.QueueName,
                basicProperties: null,
                body: message);
        }

        public void Dispose()
        {
            _channel.Dispose();
            _connection.Dispose();
        }
    }
}