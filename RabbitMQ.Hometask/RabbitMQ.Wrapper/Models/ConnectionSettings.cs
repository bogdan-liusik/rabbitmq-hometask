﻿using System;

namespace RabbitMQ.Wrapper.Models
{
    public class ConnectionSettings
    {
        public string HostName { get; set; }
        public Uri Uri { get; set; }
        public string QueueName { get; set; }
        public string ExchangeName { get; set; }
        public string RoutingKey { get; set; }
    }
}