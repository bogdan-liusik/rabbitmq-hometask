﻿namespace RabbitMQ.Wrapper.Interfaces
{
    public interface IRabbitMqService
    {
        void ListenQueue(string queueName);
        void SendMessageToQueue(byte[] message);
    }
}